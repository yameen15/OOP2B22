<?php
abstract class  Employee{
    public $firstName="";
    public $lastName="";

    public function __construct($fName,$lName)
    {
        $this->firstName=$fName;
        $this->lastName=$lName;
    }

    public function getFullName(){
        return $this->firstName." ".$this->lastName;
    }
    public abstract function getMonthlySalary();


}




class FullTimeEmployee extends Employee{

    public $annualSalary="";




    public function getMonthlySalary(){
        return $this->annualSalary/12;
    }

}
class PartTimeEmployee extends Employee{

    public $perHourPayment="";
    public $TotalHour="";



    public function getMonthlySalary(){
        return $this->perHourPayment*$this->TotalHour;
    }

}


$fullTimeEmployee= new FullTimeEmployee("Sumon","Chowdury");
echo $fullTimeEmployee->getFullName();
echo "<br>";
$partTimeEmployee= new PartTimeEmployee("Nayeem","Angel");
echo $partTimeEmployee->getFullName();

