<?php
//trait SayWorld {
//    public function sayHello() {
//        echo "someThing";
//    }
//}
//class Base {
//    public function sayHello() {
//        echo 'Hello ';
//    }
//}
//
//
//
//class MyHelloWorld extends Base {
//    use SayWorld;
//    public function sayHello() {
//        echo 'new ';
//    }
//}
//
//$o = new MyHelloWorld();
//$o->sayHello();

trait A {
    public function smallTalk() {
        echo 'a';
    }

}

trait B {
    public function smallTalk() {
        echo 'b';
    }

}

class Talker {
    use A, B {
        A::smallTalk insteadof B;
        //A::bigTalk insteadof B;
    }
}

$obj= new Talker();
$obj->smallTalk();


?>
